function growth(cycleComplete) {
  return startingValue * (Math.pow(growthFactor, (cycleComplete / numberOfCycles)));
}

function cycleOfNextHighestInt(range, oldHighestInt) {
  if (oldHighestInt < 0) return -1;
  for (var i = 0; i < range.length; i++) {
    if (range[i] > oldHighestInt) return i;
  }
  return -1;
}

function valueForCycle(range, cycle) {
  if (cycle < 0) return -1;
  return range[cycle];
}

function getOrdinal(n) {
  var s = ["th", "st", "nd", "rd"],
    v = n % 100;
  return n + (s[(v - 20) % 10] || s[v] || s[0]);
} // HT https://ecommerce.shopify.com/c/ecommerce-design/t/ordinal-number-in-javascript-1st-2nd-3rd-4th-29259

var metric = "Study Circle Collaborators in Town XYZ";
var startingValue = 7;
var growthFactor = 10;
var numberOfCycles = 20;

var cycleDates = [
  "(null)",
  "May 2016 - Jul 2016",
  "Aug 2016 - Oct 2016",
  "Nov 2016 - Jan 2017",
  "Feb 2017 - Apr 2017",
  "May 2017 - Jul 2017",
  "Aug 2017 - Oct 2017",
  "Nov 2017 - Jan 2018",
  "Feb 2018 - Apr 2018",
  "May 2018 - Jul 2018",
  "Aug 2018 - Oct 2018",
  "Nov 2018 - Jan 2019",
  "Feb 2019 - Apr 2019",
  "May 2019 - Jul 2019",
  "Aug 2019 - Oct 2019",
  "Nov 2019 - Jan 2020",
  "Feb 2020 - Apr 2020",
  "May 2020 - Jul 2020",
  "Aug 2020 - Oct 2020",
  "Nov 2020 - Jan 2021",
  "Feb 2021 - Apr 2021"
]

var m = [20, 20, 20, 80];
var width = 500 - m[1] - m[3];
var height = 300 - m[0] - m[2];

var cycmin = 0; // cycle
var cycmax = 20; // cycle
var sample0 = 700;



function update() {
  var cycles = [];
  var values = [];
  var roundedToTenths = [];
  var reachedIntegers = [];
  for (var i = 0; i <= numberOfCycles; i++) {
    cycles[i] = i; // populate with 0 ... 20
    values[i] = growth(i);
    roundedToTenths[i] = Math.round(values[i] * 10) / 10;
    reachedIntegers[i] = Math.floor(roundedToTenths[i]); // This admittedly strange rounding approach is taken to force 1.99 to be interpretted as 2.
  }

  var benchmarks = [
    []
  ];
  var bmIndex = 0;
  var currentCycle = 0;
  var highestInt = reachedIntegers[0];
  while (cycleOfNextHighestInt(reachedIntegers, highestInt) >= 0) {
    currentCycle = cycleOfNextHighestInt(reachedIntegers, highestInt);
    highestInt = valueForCycle(reachedIntegers, currentCycle);
    benchmarks[bmIndex] = [
      currentCycle,
      highestInt
    ];
    bmIndex++;
  }

  var x0 = d3.scale.linear().domain([0, sample0]).range([cycmin, cycmax]);
  var data0 = d3.range(sample0).map(function(d) {
    return {
      cycle: x0(d),
      number: growth(x0(d))
    };
  });

  var x0 = d3.scale.linear().domain([cycmin, cycmax]).range([0, width]);
  var y0 = d3.scale.linear().domain([0, growth(numberOfCycles)]).range([0, width]).range([height, 0]);

  var line0 = d3.svg.line()
    .x(function(d) {
      return x0(d.cycle);
    })
    .y(function(d) {
      return y0(d.number);
    });

  var graph0 = d3.select("#graph0")
    .append("svg")
    .attr("width", width + m[1] + m[3])
    .attr("height", height + m[0] + m[2])
    .append("g")
    .attr("transform", "translate(" + m[3] + "," + m[0] + ")");

  var xAxis0 = d3.svg.axis().scale(x0).tickSize(-height).tickSubdivide(true);

  graph0.append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis0);

  var yAxisLeft0 = d3.svg.axis().scale(y0).orient("left").tickFormat(d3.format('s'));

  graph0.append("g")
    .attr("class", "y axis")
    .attr("transform", "translate(0,0)")
    .call(yAxisLeft0);

  graph0.append("path").attr("d", line0(data0));

  $("#title").html('Growth in ' + metric);
  $(".doubleInitialSpan").html(reachedIntegers[6]);
  $(".halfFinalSpan").html(reachedIntegers[14]);
  $(".finalSpan").html(reachedIntegers[20]);
  $(".metricSpan").html(metric);

  $("#benchmarks").append("<ul>");
  for (var i = 0; i < benchmarks.length; i++) {
    $("#benchmarks").append("<li>By the end of the <strong>" + getOrdinal(benchmarks[i][0]) + "</strong> cycle (<strong>"+cycleDates[benchmarks[i][0]]+"</strong>), the number of " + metric + " reaches <strong>" + benchmarks[i][1] + "</strong>.</li>");
  }
  $("#benchmarks").append(" </ul>");
}
update();



$('input').bind('change keydown keyup', function() { // replace with button press (update button)
  //  use $(this).val() instead e.target.value
  metric = $('#metric').val();
  startingValue = $('#startVal').val();

  $("#graph0").empty();
  $("#benchmarks").empty();
  update();
});